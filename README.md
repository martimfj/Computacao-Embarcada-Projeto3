# Projeto 2 - Menos é mais
por Martim Ferreira José e Vitória Mattos


### Objetivo:
O projeto tem como objetivo desenvolver uma aplicação embarcada, que contenha otimizações energéticas, a fim de consumir o mínimo de energia possível.

### O projeto:
Para isto, a atividade proposta consistiu em fazer um LED externo piscar 4 vezes a cada 3 segundos, utilizando como fonte única de alimentação um capacitor de 4F, ficando ligado pelo maior tempo possível. E também implementar a resposta de comandos enviados via UART, sendo possível ativar ou desativar o pisca do LED, informando se o comando foi executado ou não.

### Metodologia:
Para descobrir como é possível diminuir o consumo energético da placa SAM E70, foi consultado o manual do equipamento. Nele, no tópico `6 - Power Considerations` (6.6 - Low-power Modes), são descritos os modos da placa que economizam energia, *Backup Mode*, *Spleep Mode* e *Wait Mode*.  O mais drástico e eficiente em economizar energia entre eles, é o *Backup Mode*. Este modo atinge o nível mais baixo de consumo de energia possível, pois tem periféricos desabilitados e só é "acordado" do *sleep* por meio de alguns *wake-up events*, como o do RTC (Real Time Counter) e o RTT (Real Time Timer).

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
file:///C:/Users/marti/Dropbox/Insper/5/Embarcados/Computacao-Embarcada/Documentos/SAME70.pdf
file:///C:/Users/marti/Dropbox/Insper/5/Embarcados/Computacao-Embarcada-Projeto3/B-Menos-%C3%A9-mais.pdf