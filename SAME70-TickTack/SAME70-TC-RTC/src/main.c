#include "asf.h"
#include "conf_uart_serial.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/* Infos do RTC */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      0
#define SECOND      0

/* LED */
#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIN 8 //LED da placa para testar
#define LED_PIN_MASK (1<<LED_PIN)

#define USART_COM     USART1
#define USART_COM_ID  ID_USART1

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void LED_init(int estado);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
static void USART1_init(void);

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint32_t led_counter = 0;
volatile uint32_t flag_led = 1;
volatile uint32_t flag_rtc = 0;
volatile uint32_t flag_tc = 0;

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

void TC0_Handler(void){
	volatile uint32_t ul_dummy;
	ul_dummy = tc_get_status(TC0, 0);

	UNUSED(ul_dummy);

	if(flag_led == 0){
		pmc_enable_periph_clk(LED_PIO_ID);
		pin_toggle(LED_PIO, LED_PIN_MASK);
		led_counter+=1;
	}
	
	if(led_counter == 8){
		flag_tc = 1;
		flag_led = 1;
		led_counter = 0;
		pmc_disable_periph_clk(LED_PIO_ID);
		pmc_disable_periph_clk(ID_TC0);
	}
}

void RTC_Handler(void){
	uint32_t ul_status = rtc_get_status(RTC);

	if((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	/* Time or date alarm */
	if((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM){
		rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
		flag_rtc = 1;
		flag_led = 0;
		pmc_enable_periph_clk(ID_TC0);

		uint32_t h, m, s;
		rtc_get_time(RTC, &h, &m, &s);
		s+=3;
		if (s >= 60){
			m += 1;
			m = m % 60;
			s = s %  60;
		}

		rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
		rtc_set_time_alarm(RTC, 1, h, 1, m, 1, s);
	}

	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}

void enableAlarm(void){
	uint32_t h, m, s;
	rtc_get_time(RTC, &h, &m, &s);
	s+=3;
	if (s >= 60){
		m += 1;
		m = m % 60;
		s = s %  60;
	}
	rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
	rtc_set_time_alarm(RTC, 1, h, 1, m, 1, s);
}

void USART1_Handler(void){
	uint32_t ret = usart_get_status(USART_COM);
	char c;
	
	if(ret & US_IER_RXRDY){
		int chars[5];

		usart_serial_getchar(USART_COM, &c);
		printf("Char pego: %c\n\r", c);
		
		if(c == 'L'){
			printf("Liga o LED\n");
			enableAlarm();
			rtc_enable_interrupt(RTC,  RTC_IER_ALREN);
		}
		if(c == 'D'){
			printf("Desliga o LED\n");
			rtc_disable_interrupt(RTC,  RTC_IER_ALREN);
		}
	}
}

/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

void LED_init(int estado){
	//pmc_enable_periph_clk(LED_PIO_ID);
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

static void configure_console(void){
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
	#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
	#endif
		.paritytype = CONF_UART_PARITY,
	#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
	#endif
		};

		/* Configure console UART. */
		stdio_serial_init(CONF_UART, &uart_serial_options);

		/* Specify that stdout should not be buffered. */
	#if defined(__GNUC__)
		setbuf(stdout, NULL);
	#else
		/* Already the case in IAR's Normal DLIB default configuration: printf()
		* emits one character at a time.
		*/
	#endif
}

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);// | RTC_IER_SECEN); //INTERRUPCAO
}

static void USART1_init(void){
	/* Configura USART1 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4); // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate       = 115200,
		.char_length    = US_MR_CHRL_8_BIT,
		.parity_type    = US_MR_PAR_NO,
		.stop_bits   	= US_MR_NBSTOP_1_BIT	,
		.channel_mode   = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART0 */
	sysclk_enable_peripheral_clock(USART_COM_ID);

	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART_COM, &usart_settings, sysclk_get_peripheral_hz());

	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART_COM);
	usart_enable_rx(USART_COM);

	/* map printf to usart */
	ptr_put = (int (*)(void volatile*,char))&usart_serial_putchar;
	ptr_get = (void (*)(void volatile*,char*))&usart_serial_getchar;

	/* ativando interrupcao */
	usart_enable_interrupt(USART_COM, US_IER_RXRDY);
	NVIC_SetPriority(USART_COM_ID, 4);
	NVIC_EnableIRQ(USART_COM_ID);
}

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	/* Initialize the UART console. */
	configure_console();
	USART1_init();

	/* Configura Leds */
	LED_init(1);

	/* Configura RTC */
	RTC_init();
	
	/* configura alarme do RTC */
	rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
	rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE, 1, SECOND+3);

	/* Configura TC */
	TC_init(TC0, ID_TC0, 0, 32);
	//pmc_disable_periph_clk(ID_TC0);
	
	/* Configura Flags e Var Globais */
	flag_led = 1;
	led_counter = 0;
	flag_tc = 0;
	
	/* Permite que o RTC interrompa o Backup Mode */
	pmc_set_fast_startup_input(PMC_FSMR_RTCAL);
	supc_set_wakeup_mode(SUPC, SUPC_WUMR_RTCEN_ENABLE);
	
	while (1){
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFE);
		if(!flag_rtc && !flag_tc){
			pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
			pmc_sleep(SAM_PM_SMODE_BACKUP);	
		}
	}
}

//330 ohm -> 9h57
//6,5k ohm
//uma solução era desligar a interrupção do RTC ao enviar 'D' e 
// religar e rearmar o alarme ao enviar 'L'. 